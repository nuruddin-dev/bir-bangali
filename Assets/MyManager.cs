using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyManager : MonoBehaviour
{
    public GameObject canvas, movementButtons, optionPanel;

    
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas");
        movementButtons = canvas.GetComponent<Canvas>().transform.GetChild(1).gameObject;
        optionPanel = canvas.GetComponent<Canvas>().transform.GetChild(2).gameObject;
        OptionPanelClose();
    }

    public void ShowOption()
    {
        Time.timeScale = 0;
        OptionPanelOpen();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    public void Resume()
    {
        OptionPanelClose();
        Time.timeScale = 1;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void OptionPanelOpen()
    {
        movementButtons.SetActive(false);
        optionPanel.SetActive(true);
    }

    void OptionPanelClose()
    {
        movementButtons.SetActive(true);
        optionPanel.SetActive(false);
    }

}
