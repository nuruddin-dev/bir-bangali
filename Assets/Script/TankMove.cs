﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMove : MonoBehaviour
{

    public Transform start, end;
    public static float movingspeed = 2f;
    public Vector3 currentTragete;
    public AudioSource audio;
    private Animator bossAnimator;
    public AudioClip audioClip;
    public float timecounter = 0, maxtimer = 16f;
    private SpriteRenderer helth;
    public Sprite[] helthSprite;
    public GameObject HelthShow;

    // Start is called before the first frame update
    void Start()
    {
        bossAnimator = GetComponent<Animator>();
        
        currentTragete = start.position;
        audio.clip = audioClip;
        audio.Play();
        helth = HelthShow.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Bossmanager.BossLifeEnemy -= 10; 
        }
        bossAnimator.SetBool("TankDriverIsAlive", Bossmanager.tankDriverIsAlive);
        bossAnimator.SetBool("TankEnemyIsAlive", Bossmanager.tankEnemyIsAlive);
        // tank sound
        if (enabled && timecounter > maxtimer)
        {
            timecounter = 0;
            audio.Play();
        }
        else if(timecounter > maxtimer) {
            timecounter = 0;
        }

        timecounter += Time.deltaTime;

        //tank movement
        transform.position = Vector3.MoveTowards(transform.position, currentTragete, movingspeed * Time.deltaTime);
  
        if (transform.position == start.position)
        {
            currentTragete = end.position;
            transform.Rotate(0,180,0);
            //Debug.Log("position enter");
        }
        if (transform.position == end.position)
        {
            currentTragete = start.position;
            transform.Rotate(0, 180, 0);
            //Debug.Log("position enter");
        }

        if (!Bossmanager.tankEnemyIsAlive) {
            StartCoroutine("waitforme");
            bossAnimator.SetBool("TankEnemyIsAlive", Bossmanager.tankEnemyIsAlive);
            HelthShow.SetActive(false);
        }

        switch (Bossmanager.BossLifeEnemy)
        {
            case 500:
                helth.sprite = helthSprite[0];
                break;

            case 450:
                Debug.Log("Tank health 450");
                helth.sprite = helthSprite[1];
                break;

            case 400:
                helth.sprite = helthSprite[2];
                break;

            case 350:
                helth.sprite = helthSprite[3];
                break;

            case 300:
                helth.sprite = helthSprite[4];
                break;

            case 250:
                helth.sprite = helthSprite[5];
                break;

            case 200:
                helth.sprite = helthSprite[6];
                break;

            case 150:
                helth.sprite = helthSprite[7];
                break;

            case 100:
                helth.sprite = helthSprite[8];
                break;

            case 50:
                helth.sprite = helthSprite[9];
                break;

            default:
                break;

        }


    }
    private void OnBecameVisible()
    {
        audio.Play();
        //Debug.Log("Vidisnlr");
        enabled = true;

    }
    private void OnBecameInvisible()
    {
        //Debug.Log("inVidisnlr");
        audio.Stop();
    }

    public IEnumerator WaitForMe() {
        yield return new WaitForSeconds(3f);
    
    }
}
