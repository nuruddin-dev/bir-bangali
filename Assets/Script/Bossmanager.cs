﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bossmanager : MonoBehaviour
{
    public static int BossLifeEnemy = 500;
    public int BossLifeDriver = 150;
    public GameObject Player;
    
    public static bool tankDriverIsAlive = true;
    public static bool tankEnemyIsAlive = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!tankDriverIsAlive ) 
        {
            TankMove.movingspeed = 0f;
        }

        if (BossLifeEnemy <= 0)
        {
            tankEnemyIsAlive = false;
        }

        if (BossLifeDriver <= 0)
        {
            tankDriverIsAlive = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        //tanklife
        if(collision.gameObject.tag == "PlayerBulet" && transform.tag =="Enemy")
        {
            BossLifeEnemy -= 10;
            BossLifeDriver -= 10;
            Debug.Log("Tank life: "+BossLifeDriver);
        }
        //tankdriver life
        if (collision.gameObject.tag == "PlayerBulet" && transform.tag == "Trankdeiver" && BossLifeDriver>0)
        {
            BossLifeDriver -= 10;
            Debug.Log("Driver life: "+BossLifeDriver);
        }
    }
}
