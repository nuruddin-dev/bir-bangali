﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonFire : MonoBehaviour
{
    public GameObject cannonBall;
    public AudioSource FireAudio;
    public ParticleSystem cannonFireSpark;

    public bool animationFlag = false;
    private float timer = 0, maxTime = 4f;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(cannonBall, transform.position, transform.rotation);


    }

    // Update is called once per frame
    void Update()
    {
        if (timer > maxTime)
        {
            FireAudio.Play();

           
            Instantiate(cannonBall, transform.position, transform.rotation);

            
            cannonFireSpark.Play();

            timer = 0;
            StartCoroutine("waitforme");

         
        }
        
        
        timer += Time.deltaTime;
        
    }



    public IEnumerator waitforme() {
        
        if (Bossmanager.tankDriverIsAlive)
        {
            TankMove.movingspeed *= -2;
            yield return new WaitForSeconds(.01f);
            TankMove.movingspeed = 2;

        }
        
    }

   
}
