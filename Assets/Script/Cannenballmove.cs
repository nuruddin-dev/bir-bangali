﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannenballmove : MonoBehaviour
{
    
    public float ballspeed = 15f;
    float timer = 0, maxtime = 1.5f;
    bool blastflag = true;
    
    


    
    // Start is called before the first frame update
    void Start()
    {

        GetComponent<Rigidbody2D>().AddForce( transform.right * ballspeed); 
    }


    private void Update()
    {
        if (timer > maxtime) { 
           
            Destroy(gameObject);
        }
        timer += Time.deltaTime;
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if (collision.gameObject.tag == "Field" && blastflag)
        {
            Playblast();
        }
        if (collision.gameObject.tag == "Player") { 

            
            //Player life will -- 50%
        }
        

    }

   
    void Playblast() {
        blastflag = false;
        GetComponent<AudioSource>().Play();
       
    }

    
}
