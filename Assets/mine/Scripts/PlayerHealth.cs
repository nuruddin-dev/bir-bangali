﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerHealth : MonoBehaviour
{

    
    private SpriteRenderer helth;
    public Sprite[] helthSprite;
    public GameObject HelthShow;

    // Start is called before the first frame update
    void Start()
    {
       
        helth = HelthShow.GetComponent<SpriteRenderer>();
    }


    // Update is called once per frame
    void Update()
    {
       
        // switch (Player.playerHealth)
        switch(gameObject.GetComponent<Player>().playerHealth)
        {

            case 100:
                //Debug.Log("aaa");
                helth.sprite = helthSprite[0];
                break;

            case 90:
                helth.sprite = helthSprite[1];
                break;

            case 80:
                helth.sprite = helthSprite[2];
                break;

            case 70:
                helth.sprite = helthSprite[3];
                break;

            case 60:
                helth.sprite = helthSprite[4];
                break;

            case 50:
                helth.sprite = helthSprite[5];
                break;

            case 40:
                helth.sprite = helthSprite[6];
                break;

            case 30:
                helth.sprite = helthSprite[7];
                break;

            case 20:
                helth.sprite = helthSprite[8];
                break;

            case 10:
                helth.sprite = helthSprite[9];
                break;

            case 0:
                helth.gameObject.SetActive(false);
                break;

            default:
                break;

        }


    }
}
