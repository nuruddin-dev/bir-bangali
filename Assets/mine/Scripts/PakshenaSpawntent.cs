﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PakshenaSpawntent : MonoBehaviour
{
    public GameObject player;
    public GameObject enemyfromtent;
    public int numberOfEnemy = 5;
    private bool allEnemyOut;
    private bool canEnemyOut;
    private float distance;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        canEnemyOut = true;
        //Debug.Log(player.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(transform.position);
        distance = Vector2.Distance(player.transform.position,transform.position);
        
        if (distance < 10 && distance > 0 && !allEnemyOut && canEnemyOut)
        {
            Debug.Log("if called");
            SpawnEnemy();
        }
        if(numberOfEnemy==0)
        {
            allEnemyOut = true;
        }

    }

    IEnumerator EnemySpawnTent()
    {
        canEnemyOut = false;
        Debug.Log("Ienumerator called");
        yield return new WaitForSeconds(1.2f);
        canEnemyOut = true;
    }
    void SpawnEnemy()
    {
        
        Instantiate(enemyfromtent,transform.position, transform.rotation);
        StartCoroutine("EnemySpawnTent");
        numberOfEnemy--;
    }
}
