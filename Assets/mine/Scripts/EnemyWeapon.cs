﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    private Enemy enemy;
    public GameObject bulet;
    private bool isShoot;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Wait");
        enemy = gameObject.GetComponentInParent<Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy.playerScript.IsPlayerAlive() && enemy.Distance() <= 8 && !isShoot)
        {
            bulet.tag = "EnemyBulet";
            GetComponent<AudioSource>().Play();
            Instantiate(bulet, transform.position, transform.rotation);
            StartCoroutine("Wait");
        }
    }

    private IEnumerator Wait()
    {
        isShoot = true;
        yield return new WaitForSeconds(.5f);
        isShoot = false;

    }
}
