﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : MonoBehaviour
{
    private Vector3 currentTarget;
    private Rigidbody2D boatRigidbody;
    private bool isBoatRightFaced;
    private float boatSpeed;
    private bool isPlayerOnBoat;
    private Quaternion boatDirection;
    // Start is called before the first frame update
    void Start()
    {
        boatRigidbody = GetComponent<Rigidbody2D>();
        isBoatRightFaced = true;
        isPlayerOnBoat = false;
        //transform.Rotate(0, 180, 0);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerOnBoat)
        {
            BoatMoving();
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Ground")
        {
            //isBoatRightFaced = false;
            boatSpeed = 0f;
            StartCoroutine("Wait");
        }
    }

    public IEnumerator Wait()
    {
        boatRigidbody.velocity = Vector3.zero;
        yield return new WaitForSeconds(5);
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Ground")
        {
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player" )
        {
            //transform.Rotate(0, 180, 0);
            boatSpeed = 5f;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
        }
        if (collision.transform.tag == "Player")
        {
            isPlayerOnBoat = true;
            boatDirection = collision.transform.rotation;
        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            isPlayerOnBoat = false;
            boatSpeed = 5f;
        }
    }

    void BoatMoving()
    {
        //boatRigidbody.velocity = new Vector2(boatSpeed, boatRigidbody.velocity.y);

        Debug.Log("BoatMoving is called.");
        if (isBoatRightFaced)
        {
            Debug.Log("BoatMoving is boatrightfaced called.");
            boatRigidbody.velocity = new Vector2(boatSpeed, boatRigidbody.velocity.y);
            //isBoatRightFaced = false;
        }
        else
        {
            Debug.Log("BoatMoving is boatLeftfaced called.");
            boatRigidbody.velocity = new Vector2(-boatSpeed, boatRigidbody.velocity.y);
            //isBoatRightFaced = true;
        }   
        //boatRigidbody.SetRotation(boatDirection);
        //boatRigidbody.position += new Vector2(boatRigidbody.transform.position.x* Time.deltaTime, boatRigidbody.transform.position.y);
        //boatRigidbody.AddForce(Vector3.forward, ForceMode2D.Impulse);
    }
}
