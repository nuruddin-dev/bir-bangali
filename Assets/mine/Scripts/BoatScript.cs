﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoatScript : MonoBehaviour
{
    private bool isPlayerOnBoat;
    private bool isBoatAtRight;
    private bool isBoatMoveRight;
    private static int x = 2;
    public GameObject startBoatButton;
    public Animator boatAnim;


    // Start is called before the first frame update
    void Start()
    {
    	startBoatButton = GameObject.Find("Canvas").GetComponent<Canvas>().transform.GetChild(0).gameObject;
        startBoatButton.SetActive(false);
        boatAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        boatAnim.SetBool("isPlayerOnBoat", isPlayerOnBoat);
        boatAnim.SetBool("isBoatAtRight", isBoatAtRight);
        boatAnim.SetBool("isBoatMoveRight", isBoatMoveRight);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.transform.tag == "Player")
        {
        	Player player = GameObject.Find("Player").GetComponent<Player>();

            if (player.isPlayerHaveOar)
                startBoatButton.SetActive(true);
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
            isPlayerOnBoat = true;
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if(collider.transform.tag == "Player")
        {
            startBoatButton.SetActive(false);
            isPlayerOnBoat = false;
        }
    }

    public void MoveTheBoat()
    {
        startBoatButton.SetActive(false);

        x++;

        if ((x % 2) == 0)
            isBoatMoveRight = false;
        else
            isBoatMoveRight = true;
    }
}
