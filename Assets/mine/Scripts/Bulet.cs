﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulet : MonoBehaviour
{

    public float buletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(transform.right * buletSpeed);
        StartCoroutine("Fire");
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
      Destroy(gameObject); 
    }

    public IEnumerator Fire()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
