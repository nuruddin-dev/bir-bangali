﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour
{
    public GameObject explosion;
 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.transform.tag == "Player")
    //    {
    //        Instantiate(explosion, transform.position, transform.rotation);
    //        explosion.GetComponent<ParticleSystem>().Play();
    //    }
    //}
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.transform.tag == "Player")
        {
            Debug.Log(gameObject.tag);
            Instantiate(explosion, transform.position, transform.rotation);
            explosion.GetComponent<ParticleSystem>().Play();
            gameObject.GetComponent<AudioSource>().Play();
            StartCoroutine("DestroyMine");
            
        }
    }

    IEnumerator DestroyMine()
    {
        yield return new WaitForSeconds(.8f);
        transform.gameObject.SetActive(false);
    }
}
