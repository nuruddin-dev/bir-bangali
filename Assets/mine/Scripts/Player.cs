﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Animator anim;
    //player health
    public int playerHealth = 100;
    //Rigidbody
    private Rigidbody2D playerRigidbody;

    //is player active
    public bool isPlayerAlive;

    //player speed 
    public float playerSpeed;

    // player direction
    private bool isPlayerRight;

    //is player grounded
    private bool isPlayerGrounded;

    //player jump force
    public float playerJumpForce;
    //player have oar
    public bool isPlayerHaveOar;

    //player can Fire
    public bool playerCanFire;
    //player on boat
    public bool isPlayerOnBoat;
    //Oar GameObject
    public GameObject Oar;
    public bool oarMissingPrompt;
    
    public GameObject promptRight, promptLeft;
    //For "forward" and "backward" button on android
    public bool forward, backward, jump;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        isPlayerHaveOar = false;
        isPlayerAlive = true;
        isPlayerRight = true;
        isPlayerGrounded = true;
        playerCanFire = true;
        isPlayerOnBoat = false;
        playerRigidbody = GetComponent<Rigidbody2D>();

        //For "forward" and "backward" button on android
        forward = false;
        backward = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (gameObject.activeSelf)
        {

            anim.SetFloat("isRunning", Mathf.Abs(playerRigidbody.velocity.x) );
            anim.SetBool("isJumping", !isPlayerGrounded);
            anim.SetBool("isOnBoat", isPlayerOnBoat);
            anim.SetBool("isPlayerAlive", isPlayerAlive);
            // jump
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow) || jump)
            {
                PlayerJump();
            }

            // forward
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || forward)
            {
                PlayerForward();
            }

            // back
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) || backward)
            {
                PlayerBackward();
            }

            if (playerHealth <= 0)
            {
                isPlayerAlive = false;
                StartCoroutine("PlyerDie");
            }
        }
    }

    

    void PlayerForward()
    {
        if (!isPlayerRight)
        {
            transform.Rotate(0, 180, 0);
            isPlayerRight = true;
        }
        
        playerRigidbody.velocity = new Vector2(playerSpeed, playerRigidbody.velocity.y);
    }

    void PlayerBackward()
    {
        if (isPlayerRight)
        {
            transform.Rotate(0, 180, 0);
            isPlayerRight = false;
        }
        playerRigidbody.velocity = new Vector2(-playerSpeed, playerRigidbody.velocity.y);
    }

    void PlayerJump()
    {
        if (isPlayerGrounded) { 
            playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, playerJumpForce);
        }
        isPlayerGrounded = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground" || collision.tag == "Stone")
        {
            isPlayerGrounded = true;
        }
        if (collision.tag == "EnemyBulet")
        {
            playerHealth -= 10;
        }
        if (collision.tag == "Oar")
        {
            Oar.SetActive(false);
            isPlayerHaveOar = true;
            oarMissingPrompt = false;
        }
        if (collision.tag == "AirDrop")
        {
            Debug.Log(playerHealth);
            playerHealth -= 50;
        }
        if (collision.tag == "Mine")
        {
            Debug.Log(playerHealth);
            playerHealth -= 20;

        }
        if (collision.tag == "River")
        {
            playerHealth -= 100;
        }
        if (collision.tag == "CanonBall")
        {
            playerHealth -= 20;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.transform.tag == "Boat")
        {
            isPlayerGrounded = true;
            isPlayerOnBoat = true;
            transform.parent = collision.transform;

            if(!isPlayerHaveOar)
                oarMissingPrompt = true;
        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        isPlayerOnBoat = false;
        isPlayerGrounded = true ;
        transform.parent = null;
    }

    public bool IsPlayerAlive()
    {
        return isPlayerAlive;
    }

    public bool IsPlayerGrounded()
    {
        return isPlayerGrounded;
    }

    IEnumerator PlyerDie()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
        GameObject.Find("SceneManager").GetComponent<MyManager>().OptionPanelOpen();
    }

    //For UI button controls in android
    public void ForwardButtonDown()
    {
        forward = true;
    }
    public void ForwardButtonUp()
    {
        forward = false;
    }
    public void BackwardButtonDown()
    {
        backward = true;
    }
    public void BackwardButtonUp()
    {
        backward = false;
    }
    public void JumpButtonDown()
    {
        jump = true;
    }
    public void JumpButtonUp()
    {
        jump = false;
    }
}
