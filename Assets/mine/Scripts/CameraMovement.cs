﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject target;
    public float followAhead;
    private Vector3 targetPosition;
    public float smoothing;
    public bool followTarget;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player");
        followTarget = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (followTarget)
        {
            //if(playerController.isGround){
            //	targetPosition = new Vector3 (target.transform.position.x,target.transform.position.y,transform.position.z);
            //}else{
            targetPosition = new Vector3(target.transform.position.x, transform.position.y, transform.position.z);
            //}

            //targetPosition = new Vector3 (target.transform.position.x,target.transform.position.y,transform.position.z);
            if (target.transform.localScale.x > 0f)
            {
                targetPosition = new Vector3(targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
            }
            else
            {
                targetPosition = new Vector3(targetPosition.x - followAhead, targetPosition.y, targetPosition.z);
            }

            //transform.position = targetPosition;
            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing * Time.deltaTime);
        }
    }
}
