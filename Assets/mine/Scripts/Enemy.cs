﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator enemyAnim;

    private GameObject player;
    // enemy direction
    private bool isEnemyRight;

    //enemy can Fire
    public bool enemyCanFire;

    //enemy health
    public int enemyHealth = 100;

    public Player playerScript;
    Rigidbody2D enemyRigidbody;
    public float enemySpeed;
    private bool enemyAutoMovement = true;
    private int rDirection = 0;
    //public GameObject bulet;
    private float distance;

    // Start is called before the first frame update
    void Start()
    {
        enemyAnim = GetComponent<Animator>();

        isEnemyRight = true;
        enemyCanFire = true;

        player = GameObject.Find("Player");
        playerScript = player.GetComponent<Player>();
        
        enemyRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //When player is alive
        if (playerScript.IsPlayerAlive())
        {
            distance = Vector2.Distance(player.transform.position, transform.position);

            //player ditect and move to player
            if (distance < 10 && distance > 8)
            {
                if (player.transform.position.x > transform.position.x)
                    GoRight();
                else
                    GoLeft();

                enemyAnim.SetBool("isFiring", false);
            }

            //stop moving and attacking player
            else if (distance <= 8)
                enemyAnim.SetBool("isFiring", true);

            //Random walk
            else
            {
                if (enemyAutoMovement)
                    StartCoroutine("MoveEnemy");
                if (rDirection >= 2)
                    GoRight();
                else
                    GoLeft();
            }
        }
        //When player is dead
        else
        {
            enemyAnim.SetBool("isFiring", false);

            if (enemyAutoMovement)
                StartCoroutine("MoveEnemy");

            if (rDirection < 2)
                GoRight();
            else
                GoLeft();
        }

        //When enemy is dead
        if (enemyHealth <= 0)
        {
            Destroy(gameObject);
        }

    }

    private void GoRight()
    {
        if (!isEnemyRight)
        {
            transform.Rotate(0, 180, 0);
            isEnemyRight = true;
        }
        enemyRigidbody.velocity = new Vector2(enemySpeed, enemyRigidbody.velocity.y);
    }

    private void GoLeft()
    {
        if (isEnemyRight)
        {
            transform.Rotate(0, 180, 0);
            isEnemyRight = false;
        }
        enemyRigidbody.velocity = new Vector2(-enemySpeed, enemyRigidbody.velocity.y);
    }

    private IEnumerator MoveEnemy()
    {
        enemyAutoMovement = false;
        yield return new WaitForSeconds(5f);
        rDirection = Random.Range(1,4);
        enemyAutoMovement = true;
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Enemy")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0f; //Because the collider will be triggered soon.
            collision.gameObject.GetComponent<Rigidbody2D>().mass = 0f;
            collision.collider.isTrigger = true; //When two enemies collide, they can go through themself.
        }
        //Here rDirection is changed to set the direction of enemy if he touch stone.
        //It is needed because when two enemies are together the trigger is on so they can go through stone.
        if(collision.transform.tag == "Stone")
        {
            if (rDirection < 2)
                rDirection = 3;
            else
                rDirection = 1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerBulet")
        {
            enemyHealth -= 20;
        }
        //It also needed when two enemies are touching stone together.
        if(collision.transform.tag == "Stone")
        {
            if (rDirection < 2)
                rDirection = 3;
            else
                rDirection = 1;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Enemy")
        {
            collider.isTrigger = false; // After going through the other enemy collider is back.
            collider.GetComponent<Rigidbody2D>().gravityScale = 2.5f;
            collider.GetComponent<Rigidbody2D>().mass = 10f;
        }
    }

    public float Distance()
    {
        return distance;
    }
}
