﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    private Player player;
    public GameObject bulet;
    public int numberOfBullets;
    private bool playerCanFire;
    
    // Start is called before the first frame update
    void Start()
    {
        player = gameObject.GetComponentInParent<Player>();
        numberOfBullets = 500;
        playerCanFire = true;

}

// Update is called once per frame
void Update()
    {
        // shoot
        if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            PlayerShoot();
        }
        if(numberOfBullets<=0)
        {
            playerCanFire = false;
        }
    }

    public void PlayerShoot()
    {
        if (player.IsPlayerGrounded() && playerCanFire)
        {
            StartCoroutine("FireAnimation");
            bulet.tag = "PlayerBulet";
            Instantiate(bulet, transform.position,transform.rotation);
            numberOfBullets--;
            GetComponent<AudioSource>().Play();
        }
    }

    IEnumerator FireAnimation() {
        player.anim.SetBool("isFiring", true);
        yield return new WaitForSeconds(0.5f);
        player.anim.SetBool("isFiring", false);

    }
}
