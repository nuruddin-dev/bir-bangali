﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public Animator blastanim;
    private bool isblasting;
    // Start is called before the first frame update
    void Start()
    {
        blastanim = GetComponent<Animator>();
        isblasting = false;
    }

    // Update is called once per frame
    void Update()
    {
        blastanim.SetBool("BombBlast", isblasting);

        if (isblasting == true)
        {
            StartCoroutine("Finish");
        }

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        isblasting = true;
        GetComponent<AudioSource>().Play();
    }

    IEnumerator Finish()
    {
        yield return new WaitForSeconds(.5F);
        transform.gameObject.SetActive(false);
    }

}
