﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour
{
    public float speed = 90;
    private Rigidbody2D plane;
    private bool isblast=false;
    public float time;

    public GameObject bomb;
    // Start is called before the first frame update
    void Start()
    {
        plane = GetComponent<Rigidbody2D>();
        //bomb.GetComponent<Rigidbody2D>();
        transform.Rotate(0, 180, 0);
    }

    // Update is called once per frame
    void Update()
    {
        plane.velocity = new Vector2(-speed * Time.deltaTime, 0f);

        if(!isblast )
        {
            StartCoroutine("Blast");
            Instantiate(bomb, transform.position, transform.rotation);
        }
       

      

       //bomb.velocity = transform.TransformDirection(Vector3.down * 10f);
    }
    IEnumerator Blast()
    {
        isblast = true;
        yield return new WaitForSeconds(time);
        isblast = false;
    }


    void OnBecameVisible()
    {
        transform.gameObject.SetActive(true);
    }
    void OnBecameInvisible()
    {
        transform.gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }

}
