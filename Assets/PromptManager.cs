using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PromptManager : MonoBehaviour
{
    public GameObject player;
    public SpriteRenderer spriteRenderer;
    public Sprite oarMissing;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.GetComponent<Player>().isPlayerOnBoat)
        {
            if(player.GetComponent<Player>().oarMissingPrompt)
            {
                gameObject.transform.position = player.GetComponent<Player>().promptRight.transform.position;
                spriteRenderer.sprite = oarMissing;
            }
        }
        else
            spriteRenderer.sprite = null;
    }
}
